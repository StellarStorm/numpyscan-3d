#!/usr/bin/env python3
import glob
import os
from shutil import copy2, move

from natsort import natsorted


class TransferFieldArrays:
    """
    Copy or move a list of files from one location to another while preserving
    directory organization.
    """
    def __init__(self, old_files, new_base_path):
        self.old_files = old_files
        self.new_base_path = new_base_path
        if len(self.old_files) > 1:
            self.target_paths = self._setup_new_dir_structure()
        else:
            fp = os.path.basename(self.old_files[0])
            self.target_paths = [os.path.join(self.new_base_path, fp)]

    def _setup_new_dir_structure(self):
        """
        Given a list or other iterable of file paths, find the parts of the
        paths that are different, then create a similar layout in a new folder.

        For example, if
        self.old_files = [
            '/same1/same2/different1/different1/file1A.txt',
            '/same1/same2/different3/different4/file1B.txt']
        and self.new_base_path = '/copy/them/here/'

        then `new_full_paths` should be returned like so:
        new_full_paths = ['/copy/them/here/different1/different2/fileA.txt',
            '/copy/them/here/different3/different4/fileB.txt']

        Returns:
            new_full_paths:     A list of file paths where the first parts of
                                each path should be a common directory path,
                                while after this common path each file should
                                have a unique path that reflects its
                                pre-existing arrangement.
        """
        common_path = os.path.commonpath(self.old_files)
        new_full_paths = []
        len_to_skip = len(common_path)
        for file_path in self.old_files:
            subpath = file_path[len_to_skip:]
            # If subpath.strip(os.sep) is not called, joining paths will fail
            # on Windows by joining only the drive letter and the subpath; i.e.
            # C:/new/path.txt instead of C:/common/path/new/path.txt
            new_path = os.path.abspath(
                os.path.join(self.new_base_path, subpath.strip(os.sep)))

            os.makedirs(os.path.dirname(new_path), exist_ok=True)
            new_full_paths.append(new_path)
        return new_full_paths

    def copy(self):
        """
        Thin wrapper around shutil.copy2() that operates on a list of paths.

        Simply copies files from `self.old_files` list to new data paths
        specified in `self.target_paths` on a one-by-one basis.

        Attempts to preserve as much metadata as possible.
        """
        for i, new_path in enumerate(self.target_paths):
            source = self.old_files[i]
            dest = new_path
            copy2(source, dest)
            print('Copied %s to %s' % (source, dest))
        print('\n')

    def move(self):
        """
        Thin wrapper around shutil.move() that operates on a list of paths.

        Simply moves files from `self.old_files` list to new data paths
        specified in `self.target_paths` on a one-by-one basis.

        Attempts to preserve as much metadata as possible by using the
        'copy2' copy function (default behavoir of shutil.move()).
        """
        for i, new_path in enumerate(self.target_paths):
            source = self.old_files[i]
            dest = new_path
            move(source, dest)
            print('Moved %s to %s' % (source, dest))
        print('\n')


def match_files(paths,
                extension='.npy',
                keywords=None,
                recursivity=True,
                require_field_in_fn=False):
    """
        Match field names to files for each field.

        Args:
            paths:                  A dictionary matching field or view names
                                    (keys) each to a single directory or folder
                                    path containing image or array files that
                                    correspond to a certain field.
            extension:              A string giving the image or array file
                                    endings. Defaults to '.npy'.
            keywords:               A list or similar iterable of strings to
                                    avoid in file names.
            recursivity:            Boolean, whether to search the given paths
                                    recursively for image or array files.
            require_field_in_fn     Boolean, whether each field name must be a
                                    component of all file names associated with
                                    that field. Useful when all files are
                                    stored in the same directory but do not all
                                    correspond to the same field.

        Return:
            matched_field_files:    A dictionary matching field or view names
                                    (keys) each to a list of all image or array
                                    files that correspond to individual fields.
        """
    scanfiles = {}
    for field in paths:
        tmp_path = os.path.join(paths[field], '*' + extension)
        tmp = glob.glob(os.path.normpath(tmp_path), recursive=recursivity)
        if require_field_in_fn:
            tmp = [fn for fn in tmp if field in fn]
        if keywords:
            for kw in keywords:
                tmp = [fn for fn in tmp if kw not in fn]
        scanfiles[field] = tmp
    # Extract file names from paths so files for the same patient may be
    # matched in `common_names`.
    scannames = {}
    for field in scanfiles:
        scannames[field] = [
            os.path.basename(fn).replace(extension, '')
            for fn in scanfiles[field]
        ]
        scannames[field] = natsorted(scannames[field])

    common_names = []
    for field in scannames:
        common_names.append(
            set([f.replace(field, '') for f in scannames[field]]))
    common_names = common_names[0].intersection(*common_names[1:])

    matched_field_files = {}
    for pt_id in common_names:
        matched_field_files[pt_id] = {}
        for field in scanfiles:
            for fn in scanfiles[field]:
                if os.path.basename(fn).replace(field,
                                                '').replace(extension,
                                                            '') == pt_id:
                    matched_field_files[pt_id][field] = fn

    return matched_field_files
