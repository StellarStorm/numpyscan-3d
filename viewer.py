"""
Easily view 3-D images of radio scans (CT, PET, MRI) that are saved as Numpy
arrays.
Also works with 4-D and 5-D numpy arrays from segmentation models, as long as
the extra dimensions either (a) are channels or (b) can be gotten rid of by
numpy.squeeze()

Copyright (C) 2018-2020 S. Gay and licensed under the MIT license
"""
from copy import deepcopy
import ctypes
import os
from platform import platform, system
import sys
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from natsort import natsorted
import numpy as np
import SimpleITK as sitk

from PyQt5 import QtWidgets
from NSui import Ui_MainWindow

from file_utils import match_files
from plot_utils import CreateHist


class Gui(QtWidgets.QMainWindow):
    """
    Handles GUI tasks
    """
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.pushButtonLoadData.clicked.connect(self.load_data)
        self.ui.pushButtonReset.clicked.connect(self.reset_state)
        self.ui.actionShowIntHist.triggered.connect(self.load_hist)
        self.ui.checkBoxChannelNumber.clicked.connect(self._inpt_for_channel)
        self.ui.actionSave_plot.triggered.connect(self.save_img)
        self.ui.listWidget.clicked.connect(self.plot)

        self.pwd = os.getcwd()
        self.patients = {}
        self.current_patient_name = None
        self.transfer_dir_1 = None
        self.transfer_dir_2 = None
        self.slice = 0
        self.dim_order = 'zxy'

        self.ui.horizontalSlider.valueChanged.connect(self.slider_index)
        self.ui.lineEditKeyword.setText('mask')

        self.figure = Figure()
        self.figure.patch.set_visible(False)
        self.canvas = FigureCanvas(self.figure)
        self.canvas.setSizePolicy(QtWidgets.QSizePolicy.Minimum,
                                  QtWidgets.QSizePolicy.Minimum)
        self.canvas.updateGeometry()
        self.ui.layoutViewer.addWidget(self.canvas)

    def slider_index(self):
        # Don't do anything unless a patient is loaded
        if self.current_patient_name:
            self.slice = int(self.ui.horizontalSlider.value())
            self.ui.lcdNumber.display(self.slice)
            self.plot()

    def match_field_to_directory(self, field_num, open_in):
        """
        For each of the number of fields or views the user has specified,
        prompt for and return the name of the field and the path to the
        directory containing the files.

        Args:
            field_num:      An integer, the number of the field (from 1 to the
                            last) that is being selected
            open_in:        A string or similar object useable in QFileDialog
                            that provides the initial directory in which to
                            open a file dialog

        Returns:
            field_name:     A user-specified string of the field name for files
                            found in the returned `path_to_dir` directory path
            path_to_dir:    A string, the path to the directory containing files
                            corresponding to `field_name`
        """
        field_name, ok = QtWidgets.QInputDialog.getText(
            self, 'Field %i' % field_num, 'Enter the view or field name')
        if ok:
            field_name = str(field_name)
            msg = 'Select folder containing the %s files' % field_name
            path_to_dir = QtWidgets.QFileDialog.getExistingDirectory(
                self, msg, open_in)
            return field_name, path_to_dir
        return field_name, ''

    def load_data(self):
        """
        Attempt to load data files for plotting. When possible, fail gracefully
        if something is not set or the user presses Cancel in a dialog instead
        of crashing.
        """
        try:
            num_fields = int(self.ui.lineEditFieldNum.text())
        except ValueError:
            return
        main_file_paths = {}
        open_file_num = num_fields
        if num_fields != 1:
            multi_msg = 'Are each of the %i fields stored in a separate file?' % num_fields
            multi = QtWidgets.QMessageBox.question(self, 'Multiple Files',
                                                   multi_msg)
            if multi == QtWidgets.QMessageBox.No:
                open_file_num = 1

        for i in range(open_file_num):
            field, path = self.match_field_to_directory(i + 1, self.pwd)
            # Skip if user pressed 'Cancel' when setting field name
            if path != '':
                main_file_paths[field] = path
        try:
            keywords = str(self.ui.lineEditKeyword.text()).split(',')
            keywords = [kw.strip() for kw in keywords]
        except ValueError:  # Raised when no keyword is set
            keywords = None
        recursive_search = self.ui.checkBoxRecursive.isChecked()
        require_field_in_fn = False
        if num_fields > 1 and open_file_num > 1:
            require_field_in_fn = True

        if self.ui.radioButtonNifti.isChecked():
            ext = '.nii.gz'
        else:
            ext = '.npy'
        self.patients = match_files(main_file_paths,
                                    extension=ext,
                                    keywords=keywords,
                                    recursivity=recursive_search,
                                    require_field_in_fn=require_field_in_fn)
        self.ui.listWidget.addItems(natsorted(self.patients.keys()))

    def threshold_img(self, array, index):
        if self.ui.radioButtonThresholdAll.isChecked():
            array = self._threshold(array)
        elif self.ui.lineEditThresholdSingle.text() != '':
            if all([
                    self.ui.radioButtonThresholdSingle.isChecked(),
                    index + 1 == int(self.ui.lineEditThresholdSingle.text())
            ]):
                array = self._threshold(array)
        return array

    def _threshold(self, array):
        output = deepcopy(array)
        if self.ui.lineEditBoolThreshold.text() != '':
            output = output >= float(self.ui.lineEditBoolThreshold.text())
        else:
            if self.ui.lineEditMinThreshold.text() != '':
                min_intensity = float(self.ui.lineEditMinThreshold.text())
                output[array < min_intensity] = min_intensity
            if self.ui.lineEditMaxThreshold.text() != '':
                max_intensity = float(self.ui.lineEditMaxThreshold.text())
                output[output > max_intensity] = max_intensity
        return output

    def _load_channel(self, array):
        channel_idx = int(self.ui.lineEditChannelNumber.text())
        return array[..., channel_idx]

    def _inpt_for_channel(self):
        if self.ui.checkBoxChannelNumber.isChecked():
            self.ui.radioButtonChannelsAll.setEnabled(True)
            self.ui.radioButtonChannelsMulti.setEnabled(True)
        else:
            self.ui.radioButtonChannelsAll.setChecked(False)
            self.ui.radioButtonChannelsMulti.setChecked(False)
            self.ui.radioButtonChannelsAll.setEnabled(False)
            self.ui.radioButtonChannelsMulti.setEnabled(False)

    def _open_file(self, file_path):
        if self.ui.radioButtonNifti.isChecked():
            nii = sitk.ReadImage(file_path)
            return sitk.GetArrayFromImage(nii).astype(np.float32)
        return np.load(file_path).astype(np.float32)

    def split_by_channel(self, array, inpt_idx):
        """
        Select channels from certain arrays specified in the GUI
        """
        if self.ui.radioButtonChannelsMulti.isChecked():
            tmp = self.ui.lineEditChannelMulti.text()
            # Only runs if user forgets to set a channel and we need
            # to do so automatically
            if tmp == '':
                tmp = inpt_idx
                self.ui.lineEditChannelMulti.setText(str(tmp))
            if len(tmp) > 1:
                split_inputs = ','.split(tmp)
                split_inputs = [int(x) for x in split_inputs]
            else:
                split_inputs = [int(tmp)]
            if inpt_idx in split_inputs:
                return self._load_channel(array)
            return array
        return self._load_channel(array)

    def transpose(self, array):
        if self.ui.radioButtonZXY.isChecked():
            return array
        return np.transpose(array, axes=[2, 0, 1])

    def _cmap(self):
        if self.ui.radioButtonGrayScale.isChecked():
            return 'gray'
        elif self.ui.radioButtonJet.isChecked():
            return 'jet'
        return None

    def plot(self):
        """
        Display images in UI.
        """
        self.figure.clear('all')
        self.current_patient_name = self.ui.listWidget.currentItem().text()
        self.ui.actionShowIntHist.setEnabled(True)
        img_titles = list(self.patients[self.current_patient_name].keys())
        img_paths = list(self.patients[self.current_patient_name].values())
        num_imgs = len(img_paths)
        cmap = self._cmap()

        contour_colors = ['red', 'yellow', 'magenta', 'cyan', 'green'] * 44
        contour_linestyles = ['solid', 'dashed', 'dashdot', 'dotted'] * 55

        # Limit columns to 3 to avoid weird image formatting
        if num_imgs <= 3:
            n_cols = num_imgs
            n_rows = 1
        elif num_imgs == 4:
            n_cols = 2
            n_rows = 2
        else:
            n_cols = 3
            n_rows = num_imgs // n_cols + 1

        if self.ui.checkBoxOverlay.isChecked():
            overlay = True
            axes = self.figure.subplots(nrows=1, ncols=1,
                                        squeeze=False).flatten()
        else:
            overlay = False
            axes = self.figure.subplots(nrows=n_rows,
                                        ncols=n_cols,
                                        squeeze=False).flatten()

        # Have to turn off axes separately in case there are more subplots than
        # images -- for example, if 7 arrays are loaded, there will be 9
        # subplots created but only 7 images shown. Without this, two empty
        # plots bounded by x and y axes would also be shown
        for ax in axes:
            ax.set_axis_off()

        max_slice = np.inf
        for i, im_file in enumerate(img_paths):
            if overlay:
                ax = axes[0]
            else:
                ax = axes[i]
            full_array = self._open_file(im_file)
            full_array = full_array.squeeze()
            full_array = self.threshold_img(full_array, i)

            if self.ui.checkBoxChannelNumber.isChecked():
                full_array = self.split_by_channel(full_array, inpt_idx=i)
            # If the user forgets to set a channel, attempt to do it
            # automatically
            elif full_array.ndim == 4:
                self.ui.checkBoxChannelNumber.setChecked(True)
                self.ui.lineEditChannelNumber.setText('0')
                if len(img_paths) > 1:
                    self.ui.radioButtonChannelsMulti.setEnabled(True)
                    self.ui.radioButtonChannelsMulti.setChecked(True)
                    self.ui.lineEditChannelMulti.setText(str(i))
                full_array = self.split_by_channel(full_array, inpt_idx=i)

            full_array = self.transpose(full_array)
            axis = 0
            # Recover if user switches between orientations where the previous
            # orientation had more slices than the new
            try:
                image = full_array[self.slice, ...]
            except IndexError:
                self.slice = full_array.shape[0] - 1
                image = full_array[self.slice, ...]

            if full_array.shape[axis] - 1 < max_slice:
                max_slice = full_array.shape[axis] - 1

            self.ui.horizontalSlider.setMaximum(max_slice)

            if i == 0:
                ax.imshow(image,
                          cmap=cmap,
                          interpolation='none',
                          vmin=full_array.min(),
                          vmax=full_array.max())
                ax.set_title(img_titles[i])
            else:
                if overlay:
                    # at this point, i >= 1 so reduce by 1 for indexing
                    ax.contour(image,
                               colors=contour_colors[i - 1],
                               linestyles=contour_linestyles[i - 1])
                else:
                    ax.imshow(image,
                              cmap=cmap,
                              interpolation='none',
                              vmin=full_array.min(),
                              vmax=full_array.max())
            ax.autoscale(enable=True, axis='both', tight=True)

        self.figure.tight_layout()
        self.canvas.draw()

    def reset_state(self):
        """
        Reset variables and the UI so we don't have to restart every time we
        want to view new images.
        """
        self.figure.clear('all')
        self.canvas.draw()
        self.ui.listWidget.clear()
        self.ui.lineEditFieldNum.clear()
        self.ui.lineEditKeyword.setText('mask')
        self.ui.lineEditBoolThreshold.clear()
        self.ui.lineEditMinThreshold.clear()
        self.ui.lineEditMaxThreshold.clear()
        self.ui.lineEditThresholdSingle.clear()
        self.ui.lineEditChannelMulti.clear()
        self.ui.radioButtonGrayScale.setChecked(True)
        self.ui.radioButtonZXY.setChecked(True)
        self.ui.radioButtonThresholdAll.setChecked(False)
        self.ui.radioButtonThresholdSingle.setChecked(False)
        self.ui.radioButtonNifti.setChecked(False)
        self.ui.radioButtonNumpy.setChecked(True)
        self.ui.checkBoxOverlay.setChecked(False)
        self.ui.checkBoxRecursive.setChecked(False)
        self.ui.checkBoxChannelNumber.setChecked(False)
        self.ui.radioButtonChannelsAll.setChecked(False)
        self.ui.radioButtonChannelsMulti.setChecked(False)
        self.ui.actionShowIntHist.setEnabled(False)
        self.patients = {}
        self.transfer_dir_1 = None
        self.transfer_dir_2 = None

    def update_after_move(self):
        """
        Remove patient ID from UI list to avoid FileNotFound error if patient
        ID is reselected, and clear display so it doesn't seem like a currently
        accessible patient is loaded
        """
        self.figure.clear('all')
        self.canvas.draw()
        self.ui.listWidget.clear()
        self.patients.pop(self.current_patient_name)
        self.current_patient_name = None
        self.ui.listWidget.addItems(self.patients.keys())
        self.ui.actionShowIntHist.setEnabled(False)

    def load_hist(self):
        """Display a histogram of array intensities in a separate window"""
        files = list(self.patients[self.current_patient_name].values())
        field_names = list(self.patients[self.current_patient_name].keys())
        imgs_array = np.asarray([np.load(fn) for fn in files])
        histogram = CreateHist(imgs_array, field_names,
                               self.current_patient_name)
        if len(files) == 1:
            histogram.create_single_plot()
        else:
            histogram.create_multi_plot()

    def save_img(self):
        """Save current display to file"""
        msg = 'Select directory for saving file'
        save_dir = QtWidgets.QFileDialog.getExistingDirectory(
            self, msg, self.pwd)
        filename = os.path.join(save_dir, self.current_patient_name)
        if os.path.exists(filename + '.png'):
            i = 1
            checkname = '{}{:d}.png'.format(filename, i)
            while os.path.exists(checkname):
                i += 1
                checkname = '{}{:d}.png'.format(filename, i)
            filename = checkname
        else:
            filename = filename + '.png'
        self.figure.savefig(filename)


def main():
    """
    Opens GUI
    """
    if system() == 'Windows':  # Display the icon in the taskbar
        if 'Vista' not in platform():
            myappid = 'numpyScan'
            ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(
                myappid)
    app = QtWidgets.QApplication.instance()
    if not app:
        app = QtWidgets.QApplication(sys.argv)
    ex1 = Gui()
    ex1.setWindowTitle('NumpyScan 3D')
    # ex1.setWindowIcon(QtGui.QIcon('ic_assessment_black_48dp_2x'))
    ex1.show()
    ex1.activateWindow()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
