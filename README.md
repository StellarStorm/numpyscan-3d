# NumpyScan 3D

View 3D images (such as CT scans or segmentation masks) saved as a single Numpy
array.

![Main Window](images/mainwin.png)

## Dependencies

(Minimum tested versions)
Python >= 3.7.6

PyQt >= 5.9.7

Numpy >= 1.18.1

Matplotlib >= 2.2.3

If you are using the Anaconda python distribution, you can load a pre-built
environment by opening a command prompt inside the provided conda folder, and
then running `conda env create -f generic_environment.yml`

Then run `activate NumpyScan` to activate the python environment, and
`deactivate` to close.

## Running

Make sure all dependencies have been installed (see [above](#dependencies)).

Start with `python viewer.py`. Depending on your operating system and how
Python 3 was installed, you may instead need to run `python3 viewer.py`.

## All files

Images must be saved as numpy arrays and their file names must end with .npy.

Sometimes multiple .npy files may exist in the same directory - for example,
image and mask files. If all the image file names contain a certain word, you
can limit NumpyScan to only show those images with the "Keyword (optional)"
text field. Type in the keyword first before choosing scan type.

## Output of segmentation models (i.e. Tensorflow U-Net)

Especially for multi-channel outputs (i.e. a categorical mask), your 3-D image
array may be saved inside a 5-D numpy array of shape
(batch_size, **image_dimsions, channels) or
(batch_size, channels, **image_dimensions)

In this case, select the "Channels Last" or "Channels First" radio button.

If you forget to do so and this program detects that your array has more than 3
dimensions, it will automatically attempt to load it with the "Channels Last"
setting.

## Copying image files

Image arrays can be copied to one of two user-specified folders:

1. First make sure an image is loaded and displayed in the viewing field.
Whichever image is displayed is the one that will be copied.

2. Click the first "Copy to" button, and choose the folder into which you want
to copy the file.

3. If you want to copy to a second folder, click the second "Copy to" button and
follow the same steps as before.

4. Now, whenever you click either button, the loaded image will be copied to the
corresponding directory. There will also be a notification in your terminal or
command prompt that shows the file copy.

## License

NumpyScan 3D is copyright (C) 2018-2020 S. Gay and licensed under the terms of
the MIT license. See License.md for more details.
