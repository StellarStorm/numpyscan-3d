"""
Handles initial setup and various tasks for embedding a matplotlib window in
Qt
"""

import matplotlib as mplib
from matplotlib.backends.backend_qt5agg import FigureCanvas
import matplotlib.style as mstyle
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import numpy as np
from PyQt5 import QtWidgets


def setup_plot_for_qt(subplot_geom=[1, 1], axis_off=True, autoscale=True):
    """
    Create any number of plots inside a single canvas to be embedded in a Qt5
    window.

    Args:
        subplot_geom:       A 1x2 list for the number of rows and number of
                            columns to be set up. Valid entries of the list are
                            integers

        axis_off:           Whether to initialize the plot(s) with axes or not.
                            Defaults to True (axes will not be shown).

        autoscale:          Use autoscale


    Returns:
        figure:             An object of class matplotlib.Figure()
        axes:               Subplots created from figure.subplots. Geometry
                            is set by subplot_geom argument
        canvas:             A Figurecanvas() object with figure as an attribute.

    """
    mplib.use('Qt5Agg')
    figure = Figure()
    axes = figure.subplots(nrows=subplot_geom[0], ncols=subplot_geom[1])

    # Use if more than one subplot has been created
    if any(i > 1 for i in subplot_geom):
        if axis_off:
            np.vectorize(lambda ax: ax.set_axis_off())(axes)
        if autoscale:
            np.vectorize(lambda ax: ax.autoscale(
                enable=True, axis='both', tight=True))(axes)
        elif not autoscale:
            np.vectorize(lambda ax: ax.autoscale(enable=False))(axes)
    # It's simpler when only one subplot is created
    else:
        if axis_off:
            axes.set_axis_off()
        if autoscale:
            axes.autoscale(enable=True, axis='both', tight=True)
        elif not autoscale:
            axes.autoscale(enable=False)

    canvas = FigureCanvas(figure)
    canvas.setSizePolicy(QtWidgets.QSizePolicy.Minimum,
                         QtWidgets.QSizePolicy.Minimum)
    canvas.updateGeometry()
    return figure, axes, canvas


def get_available_styles(exclude='_classic_test', sort=True):
    styles = mstyle.available
    if exclude:
        styles.remove(exclude)
    if sort:
        styles.sort(key=str.lower)
    return styles


class CreateHist:
    """Show matplotlib histogram for numpy arrays"""

    def __init__(self, array, field_names, window_title):
        """
        Args:
            array:          A 3-dimensional array of 2-d arrays to calculate
                            and dsplay histograms for. The first axis of the
                            array corresponds to the index of indivicual arrays.
                            Even if only one 2-d array is used, make it 3-first
                            before passing here with np.asarray([2d_array]) or
                            similar.
            field_names:    An iterable of field names to be used as titles for
                            the histogram plots.
            window_title:   A string to use as the title of the histogram
                            window.
        """
        self.array = array
        self.names = field_names
        self.window_title = window_title

    def create_single_plot(self):
        """Create and plot a single histogram in a new window."""
        fig, axes = plt.subplots(1)
        fig.suptitle('Intensity Histogram')
        axes.set_title(self.names[0])
        axes.hist(self.array[0, :, :].flatten(), bins=range(0, 300))
        self._common_tasks(fig)

    def create_multi_plot(self):
        """Create and plot multiple histograms in a new window."""
        plot_num = self.array.shape[0]
        fig, axes = plt.subplots(plot_num)
        fig.suptitle('Intensity Histogram')
        for i, ax in enumerate(axes):
            ax.set_title(self.names[i])
            ax.hist(self.array[i, :, :].flatten(), bins=range(0, 300))
        self._common_tasks(fig)

    def _common_tasks(self, fig):
        fig.tight_layout()
        fig.subplots_adjust(top=0.88)
        fig.canvas.set_window_title(self.window_title)
        fig.show()
